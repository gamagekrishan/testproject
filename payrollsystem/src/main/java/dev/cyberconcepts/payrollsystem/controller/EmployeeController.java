package dev.cyberconcepts.payrollsystem.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import dev.cyberconcepts.payrollsystem.model.Employee;

@Controller
public class EmployeeController {

	@RequestMapping(value = "/employees", method= RequestMethod.GET)
	public ModelAndView employees(){
		ModelAndView model = new ModelAndView("employees");
		return model;
	}
	
	@RequestMapping(value = "/add-employee", method= RequestMethod.GET)
	public ModelAndView addEmployeePage(){
		ModelAndView model = new ModelAndView("add-employee");
		return model;
	}
	
	@RequestMapping(value = "/add-employee", method= RequestMethod.POST)
	public ModelAndView addEmployee(@Valid @ModelAttribute("customer") Employee emp, BindingResult result){
		ModelAndView model = new ModelAndView("add-employee");
		System.out.println("name - "+emp.getName());
		System.out.println("age - "+emp.getAge());
		return model;
	}
}
