<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" >
	<link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" >
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12 panel panel-default" style="margin-top: 20px; background-color: #F5F5F5!important;" align="center">
				
				<div class="panel-heading" style="background-color: #F5F5F5!important;">
				<div class="row">
					<div class="col-md-6 ">
						<h3 align="left" class="panel-title">Employees <span class="badge">100</span></h3>
					</div>
                     <div align="right" class="col-md-6">
	                           <button class="btn btn-default" rel="add" type="button" data-toggle="tooltip" class="label-tooltip" data-original-title="Refresh list"><i class="fa fa-plus-circle" style="font-size: 20px;"></i></button>
					
					</div>
				</div>
				<div class="row">
					<table class="table">
						<thead>
							<tr>
								<th>Employee ID </th>
								<th>Name </th>
								<th>Department </th>
								<th>Category</th>
								<th>Basic Salary</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Name</td>
								<td>department</td>
								<td>category</td>
								<td>000.00</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
	</div>
	<script type="text/javascript" src="/resources/js/jquery-1.12.4.min.js"></script>
	
	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    $(function () {
        $("[rel='tooltip']").tooltip();
    });
</script>
	
</body>
</html>