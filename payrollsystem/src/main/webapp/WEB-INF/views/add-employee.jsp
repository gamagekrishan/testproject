<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" >
	<link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" >
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0-rc2/angular-material.min.css">
	
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.12.4.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/> "></script>
	
	<!-- angularJS libs -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.min.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
  	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
  	<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-114/svg-assets-cache.js"></script>
  	
  	<!-- angular material libs -->
  	<script src="https://cdn.gitcdn.link/cdn/angular/bower-material/v1.1.0-rc.5/angular-material.js"></script>
	
	<!-- angular files -->
	<script src="<c:url value="/resources/src/app.js"/>"></script>
	
</head>
<body ng-app="app">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12 panel panel-default" style="margin-top: 20px; background-color: #F5F5F5!important;">
				
				<div class="panel-heading" style="background-color: #F5F5F5!important;">
				<div class="row">
					<div class="col-md-12 " style="border-bottom: 1px solid #ccc ! important;padding-bottom:10px;">
						<h3 align="left" class="panel-title">Add Employee <span class="badge">100</span></h3>
					</div>
				</div>
				<div class="row">
				<form name="projectForm" action="<c:url value="/add-employee"/>" method="post">
				      <md-input-container class="md-block">
				        <label>Name</label>
				        <input md-maxlength="30" required="" name="name" ng-model="project.name">
				      </md-input-container>
				      
				      <md-input-container class="md-block">
				        <label>Age</label>
				        <input md-maxlength="30" required="" name="age" ng-model="project.age">
				      </md-input-container>
				      
				      <div class="row" align="right">
				      		<md-button type="submit" class="md-raised">Add</md-button>
				      	
				      		<md-button class="md-raised">Cancel</md-button>
			      		
				      </div>
			     </form>
				</div>
			</div>
		</div>
		</div>
	</div>
	
	
	
</body>
</html>