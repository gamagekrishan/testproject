'use strict';

App.controller('EmployeeController',['EmployeeService','$log', function(EmployeeService,$log){
	self = this;
	
	self.CustomerSearch = function(query){
		return CustomerService.getCustomerNameLike(query);
	};
	
	self.onSelectPart = function(item){
		$log.info("item - "+item);
	}
	
}]);