'use strict';

Capp.factory('EmployeeService', ['$http', '$q', function($http,$q){
	return {
		getChartData: function(vendId){
			return $http.get('http://localhost:8080/payrollsystem'+vendId+'/get-chart-data')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching customer names');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);